﻿using System.Linq.Expressions;

namespace GrpcServiceApp.Core.Repositories
{
    public interface IRepository<T>
        where T : IBaseEntity
    {        
        Task<IEnumerable<T>> GetAllAsync();

        Task<T> GetByIdAsync(long id);

        Task<IEnumerable<T>> GetRangeByIdsAsync(List<long> ids);

        Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);

        Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(T entity);
    }
}
