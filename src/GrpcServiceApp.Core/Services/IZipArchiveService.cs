﻿namespace GrpcServiceApp.Core.Services;

public interface IZipArchiveService
{
    void ExtractToDirectory(string archivePath, string extractDirectory);

    void ExtractToDirectory(byte[] data, string extractDirectory);

    IDictionary<string, byte[]> ExtractZip(byte[] data);

    void CreateArchiveDirectory(string extractedArchiveDirectory);

    void DeleteArchiveDirectory(string extractedArchiveDirectory);

    string GenerateDirectoryName(string extractDirectory);
}
