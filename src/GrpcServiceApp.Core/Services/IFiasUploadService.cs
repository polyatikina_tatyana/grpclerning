﻿using GrpcServiceApp.Core.Models;

namespace GrpcServiceApp.Core.Services
{
    public interface IFiasUploadService
    {
        Task<string> GetFiasLastVersion();
        Task<FiasDownloadFileInfo[]> GetFiasAllDownloadFileVersion();
        Task<FiasDownloadFileInfo> GetFiasLastDownloadFileVersion();
        Task<string> DownloadKladrArchive(FiasDownloadFileInfo fiasDownloadFileInfo, string directory);
        Task<string> DownloadGarFullArchive(FiasDownloadFileInfo fiasDownloadFileInfo, string directory);
        Task<string> DownloadGarDeltaArchive(FiasDownloadFileInfo fiasDownloadFileInfo, string directory);
        Task UploadKladrData();
        Task UploadGarAllData();
        Task UploadGarDeltaData();
    }
}
