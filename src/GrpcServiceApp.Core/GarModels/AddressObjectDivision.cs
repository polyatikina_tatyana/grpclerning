﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_ADDR_OBJ_DIVISION")]
public partial class AddressObjectDivision : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ID")]
    public long Id { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PARENTID")]
    public long ParentID { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHILDID")]
    public long ChildId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHANGEID")]
    public long ChangeId { get; set; }
}
