﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_ROOM_TYPES")]
public partial class RoomType : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ID")]
    public long Id { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NAME")]
    public string Name { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("SHORTNAME")]
    public string ShortName { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("DESC")]
    public string Desc { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("UPDATEDATE", DataType = "date")]
    public DateTime UpdateDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("STARTDATE", DataType = "date")]
    public DateTime StartDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ENDDATE", DataType = "date")]
    public DateTime EndDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ISACTIVE")]
    public bool IsActive { get; set; }
}
