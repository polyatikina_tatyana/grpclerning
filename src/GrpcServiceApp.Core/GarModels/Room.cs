﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_ROOMS")]
public partial class Room : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ID")]
    public long Id { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTID")]
    public long ObjectId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTGUID")]
    public string ObjectGuid { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHANGEID")]
    public long ChangeId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NUMBER")]
    public string Number { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ROOMTYPE", DataType = "integer")]
    public string RoomType { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OPERTYPEID")]
    public long OperTypeId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PREVID")]
    public long PrevId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NEXTID")]
    public long NextId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("UPDATEDATE", DataType = "date")]
    public DateTime UpdateDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("STARTDATE", DataType = "date")]
    public DateTime StartDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ENDDATE", DataType = "date")]
    public DateTime EndDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ISACTUAL")]
    public bool IsActual { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ISACTIVE")]
    public bool IsActive { get; set; }
}
