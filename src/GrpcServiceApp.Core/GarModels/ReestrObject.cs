﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_REESTR_OBJECTS")]
public partial class ReestrObject : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTID")]
    public long Id { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CREATEDATE", DataType = "date")]
    public DateTime CreateDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHANGEID")]
    public long ChangeId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("LEVELID")]
    public long LevelId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("UPDATEDATE", DataType = "date")]
    public DateTime UpdateDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTGUID")]
    public string ObjectGuid { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ISACTIVE")]
    public bool IsActive { get; set; }
}
