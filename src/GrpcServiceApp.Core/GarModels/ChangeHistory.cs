﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_CHANGE_HISTORY")]
public partial class ChangeHistory : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHANGEID")]
    public long Id { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTID")]
    public long ObjectId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ADROBJECTID")]
    public string AdrObjectId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OPERTYPEID")]
    public long OperTypeId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NDOCID")]
    public long NdocId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHANGEDATE", DataType = "date")]
    public DateTime ChangeDate { get; set; }
}
