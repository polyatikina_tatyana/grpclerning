﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_NORMATIVE_DOCS")]
public partial class NDoc : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ID")]
    public long Id { get; set; }

    //// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NAME")]
    public string Name { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("DATE", DataType = "date")]
    public DateTime Date { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NUMBER")]
    public string Number { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("TYPE")]
    public long Type { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("KIND")]
    public long Kind { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("UPDATEDATE", DataType = "date")]
    public DateTime UpdateDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ORGNAME")]
    public string OrgName { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("REGNUM")]
    public string RegNum { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("REGDATE", DataType = "date")]
    public DateTime RegDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ACCDATE", DataType = "date")]
    public DateTime AccDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("COMMENT")]
    public string Comment { get; set; }
}
