﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_HOUSES")]
public partial class House : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ID")]
    public long Id { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTID")]
    public long ObjectId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTGUID")]
    public string ObjectGuid { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHANGEID")]
    public long ChangeId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("HOUSENUM")]
    public string HouseNum { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ADDNUM1")]
    public string AddNum1 { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ADDNUM2")]
    public string AddNum2 { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("HOUSETYPE", DataType = "integer")]
    public string HouseType { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ADDTYPE1", DataType = "integer")]
    public string AddType1 { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ADDTYPE2", DataType = "integer")]
    public string AddType2 { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OPERTYPEID")]
    public long OperTypeId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PREVID")]
    public long PrevId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NEXTID")]
    public long NextId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("UPDATEDATE", DataType = "date")]
    public DateTime UpdateDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("STARTDATE", DataType = "date")]
    public DateTime StartDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ENDDATE", DataType = "date")]
    public DateTime EndDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ISACTUAL")]
    public bool IsActual { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ISACTIVE")]
    public bool IsActive { get; set; }
}
