﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_ADD_HIERARCHY")]
public partial class AdmHierarchy : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ID")]
    public long Id { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTID")]
    public long ObjectId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PARENTOBJID")]
    public long ParentObjectId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHANGEID")]
    public long ChangeId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("REGIONCODE")]
    public string RegionCode { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("AREACODE")]
    public string AreaCode { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CITYCODE")]
    public string CityCode { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PLACECODE")]
    public string PlaceCode { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PLANCODE")]
    public string PlanCode { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("STREETCODE")]
    public string StreetCode { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PREVID")]
    public long PrevId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NEXTID")]
    public long NextId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("UPDATEDATE", DataType = "date")]
    public DateTime UpdateDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("STARTDATE", DataType = "date")]
    public DateTime StartDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ENDDATE", DataType = "date")]
    public DateTime EndDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ISACTIVE")]
    public bool IsActive { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PATH")]
    public string Path { get; set; }
}