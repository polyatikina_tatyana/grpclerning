﻿namespace GrpcServiceApp.Core.GarModels;

[System.Xml.Serialization.XmlRoot("AS_MUN_HIERARCHY")]
public partial class MunHierarchy : IBaseEntity
{
    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ID")]
    public long Id { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OBJECTID")]
    public long ObjectId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PARENTOBJID")]
    public long ParentObjectId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("CHANGEID")]
    public long ChangeId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("OKTMO")]
    public string OKTMO { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PREVID")]
    public long PrevId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("NEXTID")]
    public long NextId { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("UPDATEDATE", DataType = "date")]
    public DateTime UpdateDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("STARTDATE", DataType = "date")]
    public DateTime StartDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ENDDATE", DataType = "date")]
    public DateTime EndDate { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("ISACTIVE")]
    public bool IsActive { get; set; }

    /// <remarks/>
    [System.Xml.Serialization.XmlAttribute("PATH")]
    public string Path { get; set; }
}
