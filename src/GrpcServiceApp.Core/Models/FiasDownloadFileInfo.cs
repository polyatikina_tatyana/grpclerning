﻿namespace GrpcServiceApp.Core.Models;

public class FiasDownloadFileInfo
{
    public string VersionId { get; set; }
    public string TextVersion { get; set; }
    public string Kladr47ZUrl { get; set; }
    public string GarXMLFullURL { get; set; }
    public string GarXMLDeltaURL { get; set; }
    public DateTime Date { get; set; }
}
