﻿namespace GrpcServiceApp.Core;

public interface IBaseEntity
{
    long Id { get; set; }
}
