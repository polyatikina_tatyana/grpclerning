﻿using GrpcServiceApp.Core.Services;

namespace GrpcServiceApp.Workers
{
    public class FiasUploadWorker : BackgroundService
    {
        private readonly IFiasUploadService _fiasUploadService;

        public FiasUploadWorker(IFiasUploadService fiasUploadService)
        {
            _fiasUploadService = fiasUploadService;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Delay(TimeSpan.FromSeconds(15));

            while (!stoppingToken.IsCancellationRequested)
            {
                await _fiasUploadService.UploadGarAllData();

                await Task.Delay(TimeSpan.FromDays(7));
            }
        }
    }
}
