﻿using Google.Protobuf.WellKnownTypes;
using Grpc.Core;
using GrpcServiceApp.Core.Services;

namespace GrpcServiceApp.Services
{
    /// <summary>
    /// Сервис для получения информации из ФИАС
    /// </summary>
    public class FiasApiService : FiasApi.FiasApiBase
    {
        private readonly IFiasUploadService _fiasUploadService;

        public FiasApiService(IFiasUploadService fiasUploadService)
        {
            _fiasUploadService = fiasUploadService;
        }

        public override async Task<FiasVersionReply> GetFiasVersion(Empty request, ServerCallContext context)
        {
            var lastVersion = await _fiasUploadService.GetFiasLastVersion();

            return new FiasVersionReply
            {
                Version = lastVersion
            };
        }
    }
}
