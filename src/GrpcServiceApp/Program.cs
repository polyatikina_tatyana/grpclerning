using GrpcServiceApp;
using Serilog;

var currentEnv = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");
var configuration = new ConfigurationBuilder()
            .AddJsonFile("appsettings.json")
            .AddJsonFile($"appsettings.{currentEnv}.json", optional: true)
            .AddEnvironmentVariables()
            .AddCommandLine(args)
            .Build();

Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .WriteTo.Console()
                .WriteTo.File("\\Logs\\log-.log", rollingInterval: RollingInterval.Day)
                .CreateLogger();

try
{
    Log.Warning("Starting web host. \r\n");

    using IHost host = Host.CreateDefaultBuilder(args)
        .UseSerilog((context, services, configuration) => configuration
                .WriteTo.Console()
                // But, we have access to configuration and services from the host
                .ReadFrom.Configuration(context.Configuration)
                .ReadFrom.Services(services))
        .ConfigureWebHostDefaults(configure =>
        {
            configure.UseConfiguration(configuration);
            configure.UseStartup<Startup>();
        })
        .Build();
      
    host.Run();

    Log.Warning("Stoping web host. \r\n");
}
catch (OperationCanceledException)
{
    Log.Warning("Canceled web host. \r\n");
}
catch (Exception ex)
{
    Log.Fatal(ex, "Host terminated unexpectedly. \r\n");
}
