﻿using GrpcServiceApp.Core.Repositories;
using GrpcServiceApp.Core.Services;
using GrpcServiceApp.Domain.Services;
using GrpcServiceApp.Infrastructure.Settings;
using GrpcServiceApp.Mongo.Repositories;
using GrpcServiceApp.Mongo;
using GrpcServiceApp.Services;
using Microsoft.OpenApi.Models;
using GrpcServiceApp.Mongo.Options;
using GrpcServiceApp.Workers;

namespace GrpcServiceApp
{
    public class Startup
    {
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration) { _configuration = configuration; }


        //Настройка DI (регистрация сервисов и пр.)
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddHttpClient();

            services.Configure<FiasUploadSettings>(_configuration.GetSection(FiasUploadSettings.Position));

            services.AddGrpc(options =>
            {
                options.EnableDetailedErrors = true;
                options.MaxReceiveMessageSize = 600 * 1024 * 1024; // 600 MB
                options.MaxSendMessageSize = 600 * 1024 * 1024; // 600 MB
            }).AddJsonTranscoding();
                        
            services.ConfigureSwaggerServices();

            services.AddScoped(typeof(IRepository<>), typeof(MongoRepository<>));

            services.ConfigureMongoService();

            services.AddSingleton(serviceProvider => new MongoDbContext(new MongoConnectionSetting
            {
                ConnectionString = _configuration.GetConnectionString("MongoDb"),
                DatabaseName = "FiasDb"
            }));

            services.AddTransient<IFiasUploadService, FiasUploadService>();
            services.AddSingleton<IZipArchiveService, ZipArchiveService>();

            services.AddHostedService<FiasUploadWorker>();
        }

        // Настройка конвейеров ПО промежуточного слоя (Middleware). 
        // Middleware вызываются последовательно, поэтому порядок важен
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                // выводим информацию об ошибке, при наличии ошибки
                app.UseDeveloperExceptionPage();
            }

            // добавляем возможности маршрутизации
            app.UseRouting();

            //app.UseAuthentication();
            //app.UseAuthorization();

            if (!env.IsProduction())
            {
                app.ConfigureSwagger();
            }

            // устанавливаем адреса, которые будут обрабатываться
            app.UseEndpoints(endpoints =>
            {
                // Configure the HTTP request pipeline.
                endpoints.MapGrpcService<GreeterService>();
                endpoints.MapGrpcService<FiasApiService>();

                endpoints.MapGet("/", () => "Communication with gRPC endpoints must be made through a gRPC client. To learn how to create a client, visit: https://go.microsoft.com/fwlink/?linkid=2086909");

            });
        }
    }

    public static class StartupExtensions
    {
        public static IServiceCollection ConfigureSwaggerServices(this IServiceCollection services)
        {
            services.AddGrpcSwagger();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1",
                    new OpenApiInfo { Title = "gRPC transcoding", Version = "v1" });
            });

            return services;
        }

        public static IApplicationBuilder ConfigureSwagger(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "gRPC API V1");
            });

            return app;
        }
    }
}
