﻿namespace GrpcServiceApp.Infrastructure.Settings
{
    public class FiasUploadSettings
    {
        public const string Position = nameof(FiasUploadSettings);
        public string BaseUrl { get; set; } = "https://fias.nalog.ru";
        public string LastVersionMethod { get; set; } = "WebServices/Public/GetLastDownloadFileInfo";
        public string LastDownloadFileInfoMethod { get; set; } = "WebServices/Public/GetLastDownloadFileInfo";
        public string LastAllDownloadFileInfoMethod { get; set; } = "WebServices/Public/GetAllDownloadFileInfo";
        public string Directory { get; set; } = "D:\\TEMP\\Fias";
    }
}
