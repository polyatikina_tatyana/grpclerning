﻿namespace GrpcLerning.DataModels.Info
{
    public class FiasTable
    {
        public Guid Id { get; set; }
        public string TableName { get; set; }
        public string Description { get; set; }
        public bool CanImport { get; set; } = true;
        public DateTime? LastImport { get; set; }
        public long RowCount { get; set; }
        public decimal TotalMB { get; set; }
        public decimal UnusedMB { get; set; }
        public decimal UsedMB { get; set; }
        public DateTime Create { get; set; } = DateTime.UtcNow;
        public DateTime Modify { get; set; } = DateTime.UtcNow;
    }
}
