﻿namespace GrpcLerning.DataModels.Info
{
    public class FiasVersion
    {
        public Guid Id { get; set; }
        public string Version { get; set; }
        public DateTime Date { get; set; }
        public string Source { get; set; }
        public string Schema { get; set; }
        public string FileName { get; set; }
        public bool IsUpload { get; set; }
        public int Attempt { get; set; }
        public string Error { get; set; }
        public DateTime Create { get; set; } = DateTime.UtcNow;
        public DateTime Modify { get; set; } = DateTime.UtcNow;
    }
}