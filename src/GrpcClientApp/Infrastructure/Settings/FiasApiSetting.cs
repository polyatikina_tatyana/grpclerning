﻿namespace GrpcClientApp.Infrastructure.Settings
{
    public class FiasApiSetting
    {
        public const string Position = nameof(FiasApiSetting);
        public string Url { get; set; } = "https://localhost:7082/";
    }
}
