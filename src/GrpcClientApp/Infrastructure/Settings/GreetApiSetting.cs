﻿namespace GrpcClientApp.Infrastructure.Settings
{
    public class GreetApiSetting
    {
        public const string Position = nameof(GreetApiSetting);
        public string Url { get; set; } = "https://localhost:7082/";
    }
}
