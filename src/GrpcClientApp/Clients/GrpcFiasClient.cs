﻿using Grpc.Net.Client;
using GrpcClientApp.Infrastructure.Settings;
using Microsoft.Extensions.Options;

namespace GrpcClientApp.Clients
{
    public class GrpcFiasClient
    {
        private readonly FiasApiSetting _fiasApiSetting;

        public GrpcFiasClient(IOptions<FiasApiSetting> fiasApiOptions)
        {
            _fiasApiSetting = fiasApiOptions.Value;
        }

        public async Task<string> GetFiasVersion()
        {
            using var channel = GrpcChannel.ForAddress(_fiasApiSetting.Url);
            var fiasApiClient = new FiasApi.FiasApiClient(channel);
            var reply = await fiasApiClient.GetFiasVersionAsync(new Google.Protobuf.WellKnownTypes.Empty());

            return reply.Version;
        }
    }
}
