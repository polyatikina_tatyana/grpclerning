﻿using Grpc.Net.Client;
using GrpcClientApp.Infrastructure.Settings;
using Microsoft.Extensions.Options;

namespace GrpcClientApp.Clients
{
    public class GrpcGreetClient
    {
        private readonly GreetApiSetting _greetApiSetting;

        public GrpcGreetClient(IOptions<GreetApiSetting> greetApiOptions)
        {
            _greetApiSetting = greetApiOptions.Value;
        }

        public async Task<string> GetRequestName(string name)
        {
            using var channel = GrpcChannel.ForAddress(_greetApiSetting.Url);
            var greeterClient = new Greeter.GreeterClient(channel);
            var reply = await greeterClient.SayHelloAsync( new HelloRequest { Name = name });

            return reply.Message;
        }

    }
}
