using GrpcClientApp.Clients;
using GrpcClientApp.Infrastructure.Settings;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddRazorPages();

builder.Services.Configure<FiasApiSetting>(
    builder.Configuration.GetSection(FiasApiSetting.Position));

builder.Services.Configure<GreetApiSetting>(
    builder.Configuration.GetSection(GreetApiSetting.Position));

builder.Services.AddTransient<GrpcFiasClient>();
builder.Services.AddTransient<GrpcGreetClient>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapRazorPages();

app.Run();
