﻿using GrpcClientApp.Clients;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace GrpcClientApp.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;
        private readonly GrpcFiasClient _grpcFiasClient;
        private readonly GrpcGreetClient _grpcGreetClient;

        public IndexModel(ILogger<IndexModel> logger, GrpcFiasClient grpcFiasClient, GrpcGreetClient grpcGreetClient)
        {
            _logger = logger;
            _grpcFiasClient = grpcFiasClient;
            _grpcGreetClient = grpcGreetClient;
        }

        public string FiasVersion { get; set; } 
        public string GreetResponse { get; set; }

        public Task<string> GetFiasVersionAsync()
        {
            return _grpcFiasClient.GetFiasVersion();
        }

        public async Task OnGetAsync()
        {
            FiasVersion = await GetFiasVersionAsync();
            GreetResponse = await _grpcGreetClient.GetRequestName(nameof(GrpcClientApp));
        }
    }
}