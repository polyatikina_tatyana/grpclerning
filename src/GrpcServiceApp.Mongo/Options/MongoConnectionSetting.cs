﻿namespace GrpcServiceApp.Mongo.Options
{
    public class MongoConnectionSetting
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }
    }
}
