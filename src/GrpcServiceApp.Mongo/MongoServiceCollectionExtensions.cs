﻿using Microsoft.Extensions.DependencyInjection;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Bson.Serialization;
using MongoDB.Bson;
using GrpcServiceApp.Core.GarModels;

namespace GrpcServiceApp.Mongo
{
    public static class MongoServiceCollectionExtensions
    {
        public static IServiceCollection ConfigureMongoService(this IServiceCollection services)
        {
            BsonSerializer.RegisterSerializer(new Int64Serializer(BsonType.String));

            RegisterClassMaps();
            return services;
        }

        private static void RegisterClassMaps()
        {
            MongoConventionsConfigurator.CreateDefault();

            BsonClassMap.RegisterClassMap<AddHouseType>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<AddressObject>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<AddressObjectDivision>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<AddressObjectParam>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<AddressObjectType>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<AdmHierarchy>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<Apartment>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<ApartmentParam>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<ApartmentType>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<CarPlace>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<CarPlaceParam>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<ChangeHistory>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<House>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<HouseType>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<MunHierarchy>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<NDoc>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<NDocKind>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<NDocType>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<ObjectLevel>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<OperationType>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<Param>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<ParamType>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<ReestrObject>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<Room>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<RoomParam>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<RoomType>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<Stead>(map => map.AutoMap());
            BsonClassMap.RegisterClassMap<SteadParam>(map => map.AutoMap());
        }
    }
}