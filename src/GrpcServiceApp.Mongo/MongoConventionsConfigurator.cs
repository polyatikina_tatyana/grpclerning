﻿using MongoDB.Bson.Serialization.Conventions;

namespace GrpcServiceApp.Mongo
{
    public static class MongoConventionsConfigurator
    {
        public static void CreateDefault()
        {
            var pack = new ConventionPack
            {
                new CamelCaseElementNameConvention(),
            };

            ConventionRegistry.Register("CamelCase", pack, t => true);
        }
    }
}
