﻿using GrpcServiceApp.Core.Services;
using System.IO.Compression;
using System.Text;

namespace GrpcServiceApp.Domain.Services;

public class ZipArchiveService : IZipArchiveService
{
    private const string CYRILLIC_ENCODING = "cp866";
    private const string TEMP_DIR = "UnZip";

    public void ExtractToDirectory(string archivePath, string extractDirectory)
    {
        CreateArchiveDirectory(extractDirectory);

        using ZipArchive archive = ZipFile.Open(archivePath, ZipArchiveMode.Read,
            CodePagesEncodingProvider.Instance.GetEncoding(CYRILLIC_ENCODING));

        archive.ExtractToDirectory(extractDirectory);
    }

    public void ExtractToDirectory(byte[] data, string extractDirectory)
    {
        CreateArchiveDirectory(extractDirectory);

        using var archiveStream = new MemoryStream(data);
        using var archive = GenerateZipArchive(archiveStream);
        foreach (var entry in archive.Entries)
        {
            using var reader = new BinaryReader(entry.Open());
            var fileName = entry.FullName.Replace(':', '_');
            var length = (int)entry.Length;
            if (length > 0)
            {
                var fullPath = Path.Combine(extractDirectory, fileName);
                var root = Path.GetDirectoryName(fullPath);
                Directory.CreateDirectory(root);
                File.WriteAllBytes(fullPath, reader.ReadBytes((int)entry.Length));
            }
        }
    }

    public IDictionary<string, byte[]> ExtractZip(byte[] data)
    {
        var entries = new Dictionary<string, byte[]>();

        using (var archiveStream = new MemoryStream(data))
        {
            using var archive = GenerateZipArchive(archiveStream);
            foreach (var entry in archive.Entries)
            {
                using var reader = new BinaryReader(entry.Open());
                entries[entry.FullName] = reader.ReadBytes((int)entry.Length);
            }
        }

        return entries;
    }

    private static ZipArchive GenerateZipArchive(MemoryStream archiveStream) => new(archiveStream, ZipArchiveMode.Read, false,
                CodePagesEncodingProvider.Instance.GetEncoding(CYRILLIC_ENCODING));

    public void DeleteArchiveDirectory(string extractedArchiveDirectory)
    {
        if (Directory.Exists(extractedArchiveDirectory))
        {
            Directory.Delete(extractedArchiveDirectory, true);
        }
    }

    public void CreateArchiveDirectory(string extractedArchiveDirectory)
    {
        if (!Directory.Exists(extractedArchiveDirectory))
        {
            Directory.CreateDirectory(extractedArchiveDirectory);
        }
    }

    public string GenerateDirectoryName(string extractDirectory) => Path.Combine(extractDirectory, TEMP_DIR);
}
