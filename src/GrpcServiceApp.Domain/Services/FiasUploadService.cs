﻿using GrpcServiceApp.Core;
using GrpcServiceApp.Core.GarModels;
using GrpcServiceApp.Core.Models;
using GrpcServiceApp.Core.Repositories;
using GrpcServiceApp.Core.Services;
using GrpcServiceApp.Domain.Extensions;
using GrpcServiceApp.Domain.Mappers;
using GrpcServiceApp.Infrastructure.Settings;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

namespace GrpcServiceApp.Domain.Services
{
    public class FiasUploadService : IFiasUploadService
    {
        private static SemaphoreSlim _semaphoreSlim = new SemaphoreSlim(10, 10);

        private readonly ILogger<FiasUploadService> _logger;
        private readonly IHttpClientFactory _httpClientFactory;
        private readonly FiasUploadSettings _fiasUploadSettings;
        private readonly IZipArchiveService _zipArchiveService;
        private readonly IServiceProvider _serviceProvider;

        public FiasUploadService(ILogger<FiasUploadService> logger,
            IHttpClientFactory httpClientFactory,
            IOptions<FiasUploadSettings> fiasUploadOptions,
            IZipArchiveService zipArchiveService,
            IServiceProvider serviceProvider)
        {
            _logger = logger;
            _httpClientFactory = httpClientFactory;
            _fiasUploadSettings = fiasUploadOptions.Value;
            _zipArchiveService = zipArchiveService;
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Получить версию последнего архива
        /// </summary>
        /// <returns></returns>
        public async Task<string> GetFiasLastVersion()
        {            
            var request = $"{_fiasUploadSettings.BaseUrl}/{_fiasUploadSettings.LastVersionMethod}";

            using var httpClient = _httpClientFactory.CreateClient();

            _logger.LogWarning("Запрашиваем последнюю версию данных ФИАС, {method}", request);

            var httpResponse = await httpClient.GetAsync(request);

            var successResponse = httpResponse.EnsureSuccessStatusCode();

            var responseBody = await successResponse.Content.ReadAsStringAsync();

            var jObject = JObject.Parse(responseBody);

            var versionFias = jObject["VersionId"]?.ToObject<string>();

            _logger.LogWarning("Последняя версия данных ФИАС, {version}", versionFias);

            return versionFias;
        }

        /// <summary>
        /// Получить список всех архивов
        /// </summary>
        /// <returns></returns>
        public async Task<FiasDownloadFileInfo[]> GetFiasAllDownloadFileVersion()
        {
            var request = $"{_fiasUploadSettings.BaseUrl}/{_fiasUploadSettings.LastAllDownloadFileInfoMethod}";

            using var httpClient = _httpClientFactory.CreateClient();

            _logger.LogWarning("Запрашиваем все версии данных ФИАС, {method}", request);

            var httpResponse = await httpClient.GetAsync(request);

            var successResponse = httpResponse.EnsureSuccessStatusCode();

            var responseBody = await successResponse.Content.ReadAsStringAsync();

            var fiasFileInfos = JsonConvert.DeserializeObject<FiasDownloadFileInfo[]>(responseBody);

            _logger.LogWarning("Получена информация о версия данных ФИАС, {response}", responseBody);

            return fiasFileInfos;
        }

        /// <summary>
        /// Получить данные последнего архива
        /// </summary>
        /// <returns></returns>

        public async Task<FiasDownloadFileInfo> GetFiasLastDownloadFileVersion()
        {
            var request = $"{_fiasUploadSettings.BaseUrl}/{_fiasUploadSettings.LastDownloadFileInfoMethod}";

            using var httpClient = _httpClientFactory.CreateClient();

            _logger.LogWarning("Запрашиваем последнюю версию данных ФИАС, {method}", request);

            var httpResponse = await httpClient.GetAsync(request);

            var successResponse = httpResponse.EnsureSuccessStatusCode();

            var responseBody = await successResponse.Content.ReadAsStringAsync();

            var fiasFileInfos = JsonConvert.DeserializeObject<FiasDownloadFileInfo>(responseBody);

            _logger.LogWarning("Получена информация о последней версии данных ФИАС, {response}", responseBody);

            return fiasFileInfos;
        }

        public async Task<string> DownloadKladrArchive(FiasDownloadFileInfo fiasDownloadFileInfo, string directory)
        {
            var filename = Path.Combine(directory, "kladr", fiasDownloadFileInfo.VersionId, "kladr.7z");

            CreateDirectory(Path.GetDirectoryName(filename));

            if (!File.Exists(filename))
            {
                var request = fiasDownloadFileInfo.Kladr47ZUrl;

                using var httpClient = _httpClientFactory.CreateClient();

                _logger.LogWarning("Запрашиваем архив последней версии данных ФИАС в формате КЛАДР, {method}", request);

                await httpClient.DownloadFileAsync(request, filename);
            }

            return filename;
        }

        public async Task<string> DownloadGarFullArchive(FiasDownloadFileInfo fiasDownloadFileInfo, string directory)
        {
            var filename = Path.Combine(directory, "gar", fiasDownloadFileInfo.VersionId, "full","gar_xml.zip");

            CreateDirectory(Path.GetDirectoryName(filename));

            if (!File.Exists(filename))
            {
                var request = fiasDownloadFileInfo.GarXMLFullURL;

                using var httpClient = _httpClientFactory.CreateClient();

                _logger.LogWarning("Запрашиваем архив последней версии данных ФИАС в формате ГАР, {method}", request);

                await httpClient.DownloadFileAsync(request, filename);
            }

            return filename;
        }

        public async Task<string> DownloadGarDeltaArchive(FiasDownloadFileInfo fiasDownloadFileInfo, string directory)
        {
            var filename = Path.Combine(directory, "gar", fiasDownloadFileInfo.VersionId, "delta", "gar_xml.zip");

            CreateDirectory(Path.GetDirectoryName(filename));

            if (!File.Exists(filename))
            {
                var request = fiasDownloadFileInfo.GarXMLDeltaURL;

                using var httpClient = _httpClientFactory.CreateClient();

                _logger.LogWarning("Запрашиваем архив последней delta-версии данных ФИАС в формате ГАР, {method}", request);

                await httpClient.DownloadFileAsync(request, filename);
            }

            return filename;
        }

        private void CreateDirectory(string filename)
        {
            if (Directory.Exists(filename)) return;

            Directory.CreateDirectory(filename);
        }

        public async Task UploadKladrData()
        {
            var fiasDownloadFileInfo = await GetFiasLastDownloadFileVersion();
            var downloadFileName = await DownloadKladrArchive(fiasDownloadFileInfo, _fiasUploadSettings.Directory);

        }

        public async Task UploadGarAllData()
        {
            var fiasDownloadFileInfo = await GetFiasLastDownloadFileVersion();
            var downloadFileName = await DownloadGarFullArchive(fiasDownloadFileInfo, _fiasUploadSettings.Directory);

            await UploadGarArchive(downloadFileName);
        }

        public async Task UploadGarDeltaData()
        {
            var fiasDownloadFileInfo = await GetFiasLastDownloadFileVersion();
            var downloadFileName = await DownloadGarDeltaArchive(fiasDownloadFileInfo, _fiasUploadSettings.Directory);

            await UploadGarArchive(downloadFileName);
        }

        public async Task UploadGarArchive(string downloadFileName)
        {
            var extractDirectory = _zipArchiveService.GenerateDirectoryName(Path.GetDirectoryName(downloadFileName));
            if (Directory.GetDirectories(extractDirectory)?.Length > 90)
            {
                //
            }
            else
            {
                _zipArchiveService.ExtractToDirectory(downloadFileName, extractDirectory);
            }

            var files = Directory.GetFiles(extractDirectory);
            var directories = Directory.GetDirectories(extractDirectory);
            if (files.Length == 0 && directories.Length == 0) return;

            var uploadGarDictFiles = new List<UploadGarFileOption>()
            {
                new UploadGarFileOption("AS_ADDHOUSE_TYPES", "AS_HOUSE_TYPES", "HOUSETYPE", files),
                new UploadGarFileOption("AS_ADDR_OBJ_TYPES", "AS_ADDR_OBJ_TYPES", "ADDRESSOBJECTTYPE", files),
                new UploadGarFileOption("AS_APARTMENT_TYPES", "AS_APARTMENT_TYPES", "APARTMENTTYPE", files),
                new UploadGarFileOption("AS_HOUSE_TYPES", "AS_HOUSE_TYPES", "HOUSETYPE", files),
                new UploadGarFileOption("AS_NORMATIVE_DOCS_KINDS", "AS_NORMATIVE_DOCS_KINDS", "NDOCKIND", files),
                new UploadGarFileOption("AS_NORMATIVE_DOCS_TYPES", "AS_NORMATIVE_DOCS_TYPES", "NDOCTYPE", files),
                new UploadGarFileOption("AS_OBJECT_LEVELS", "AS_OBJECT_LEVELS", "OBJECTLEVEL", files),
                new UploadGarFileOption("AS_OPERATION_TYPES", "AS_OPERATION_TYPES", "OPERATIONTYPE", files),
                new UploadGarFileOption("AS_PARAM_TYPES", "AS_PARAM_TYPES", "PARAMTYPE", files),
                new UploadGarFileOption("AS_ROOM_TYPES", "AS_ROOM_TYPES", "ROOMTYPE", files)
            };

            await Task.WhenAll(uploadGarDictFiles.Select(UploadGarFileAsync));

            foreach (var region in directories)
            {
                var regionFiles = Directory.GetFiles(region);
                var uploadGarRegionFiles = new List<UploadGarFileOption>()
                {
                    new UploadGarFileOption("AS_ADDR_OBJ", "AS_ADDR_OBJ", "OBJECT", regionFiles),
                    new UploadGarFileOption("AS_ADDR_OBJ_DIVISION", "AS_ADDR_OBJ_DIVISION", "ITEM", regionFiles),
                    new UploadGarFileOption("AS_ADDR_OBJ_PARAMS", "AS_PARAMS", "PARAM", regionFiles),
                    new UploadGarFileOption("AS_ADD_HIERARCHY", "AS_ADD_HIERARCHY", "ITEM", regionFiles),
                    new UploadGarFileOption("AS_APARTMENTS", "AS_APARTMENTS", "APARTMENT", regionFiles),
                    new UploadGarFileOption("AS_APARTMENTS_PARAMS", "AS_PARAMS", "PARAM", regionFiles),
                    new UploadGarFileOption("AS_CARPLACES", "AS_CARPLACES", "CARPLACE", regionFiles),
                    new UploadGarFileOption("AS_CARPLACES_PARAMS", "AS_PARAMS", "PARAM", regionFiles),
                    new UploadGarFileOption("AS_CHANGE_HISTORY", "AS_CHANGE_HISTORY", "ITEM", regionFiles),
                    new UploadGarFileOption("AS_HOUSES", "AS_HOUSES", "HOUSE", regionFiles),
                    new UploadGarFileOption("AS_HOUSES_PARAMS", "AS_PARAMS", "PARAM", regionFiles),
                    new UploadGarFileOption("AS_MUN_HIERARCHY", "AS_MUN_HIERARCHY", "ITEM", regionFiles),
                    new UploadGarFileOption("AS_NORMATIVE_DOCS", "AS_NORMATIVE_DOCS", "NORMDOC", regionFiles),
                    new UploadGarFileOption("AS_REESTR_OBJECTS", "AS_REESTR_OBJECTS", "OBJECT", regionFiles),
                    new UploadGarFileOption("AS_ROOMS", "AS_ROOMS", "ROOM", regionFiles),
                    new UploadGarFileOption("AS_ROOMS_PARAMS", "AS_PARAMS", "PARAM", regionFiles),
                    new UploadGarFileOption("AS_STEADS", "AS_STEADS", "STEAD", regionFiles),
                    new UploadGarFileOption("AS_STEADS_PARAMS", "AS_PARAMS", "PARAM", regionFiles)
                };

                await Task.WhenAll(uploadGarRegionFiles.Select(UploadGarFileAsync));

                uploadGarRegionFiles = null;
                regionFiles = null;
            }
        }

        private async Task UploadGarFileAsync(UploadGarFileOption option)
        {
            var xmlFile = option.Files.FirstOrDefault(x => x.Contains(option.FileName));
            if (xmlFile == null) { return; }
            
            await _semaphoreSlim.WaitAsync();
            try
            {
                using var fs = new FileStream(xmlFile, FileMode.Open);
                var dt = FiasDataReader.LoadTableFromXml(fs, "v_4_01", option.SchemaName, option.TableName);
                await SaveDataAsync(dt, option.FileName);
                await fs.FlushAsync();
                dt = null;
            }
            finally
            {
                _semaphoreSlim.Release();
            }
        }

        private async Task SaveDataAsync(DataTable dt, string fileName)
        {
            if (dt == null)
            {
                return;
            }

            //найти класс по fileName
            var garModelType = GetGarModelType(fileName);
            if (garModelType == null)
            {
                return;
            }

            //конвертировать данные
            var properties = garModelType.GetProperties();
            var columnNames = PropertyMapper.GetDataColumnNames(dt);

            await Parallel.ForEachAsync(dt.AsEnumerable(), new ParallelOptions { MaxDegreeOfParallelism = 20 },
            async (row, cancelationToken) =>
            {
                var garModel = Activator.CreateInstance(garModelType);

                foreach (var prop in properties)
                {
                    if (cancelationToken.IsCancellationRequested) return;

                    PropertyMapper.Map(garModelType, row, columnNames, prop, garModel);
                }

                //сохранить данные в БД
                await Task.Delay(1000).ConfigureAwait(false);

                garModel = null;
            }).ConfigureAwait(false);

            garModelType = null;
            properties = null;
            columnNames = null;
        }
        
        private static Type GetGarModelType(string fileName)
        {
            Assembly assembly = typeof(HouseType).Assembly;

            return assembly.GetTypes()
                .FirstOrDefault(type => type.IsClass && !type.IsAbstract &&
                               typeof(IBaseEntity).IsAssignableFrom(type) &&
                               type.GetCustomAttribute<XmlRootAttribute>()?.ElementName == fileName);
        }

        record UploadGarFileOption(string FileName, string SchemaName, string TableName, string[] Files);
    }
}
