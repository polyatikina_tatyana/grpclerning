﻿using System.Data;
using System.Globalization;
using System.Reflection;
using GrpcServiceApp.Domain.Extensions;

namespace GrpcServiceApp.Domain.Mappers
{
    public static class PropertyMapper
    {
        public static TEntity Map<TEntity>(this DataRow row)
            where TEntity : class, new()
            => Map(row, new TEntity());

        public static TEntity Map<TEntity>(this DataRow row, TEntity entity, PropertyInfo[] properties)
            where TEntity : class, new()
        {
            foreach (var prop in properties)
            {
                Map(typeof(TEntity), row, prop, entity);
            }

            return entity;
        }
                
        public static TEntity Map<TEntity>(this DataRow row, TEntity entity)
            where TEntity : class, new()
            => row.Map(entity, typeof(TEntity).GetProperties());

        public static IEnumerable<TEntity> Map<TEntity>(this DataTable table)
            where TEntity : class, new()
            => Map(table, new List<TEntity>());

        public static IEnumerable<TEntity> Map<TEntity>(this DataTable table, IList<TEntity> entities)
            where TEntity : class, new()
        {
            var properties = typeof(TEntity).GetProperties();

            foreach (DataRow row in table.Rows)
            {
                entities.Add(row.Map(new TEntity(), properties));
            }

            return entities;
        }

        public static void Map(Type type, DataRow row, PropertyInfo prop, object entity)
        {
            var columnNames = GetDataColumnNames(row.Table);

            Map(type, row, columnNames, prop, entity);
        }

        public static void Map(Type type, DataRow row, string[] columnNames, PropertyInfo prop, object entity)
        {
            var xmlName = AttributeHelper.GetXmlName(type, prop.Name);

            foreach (var columnName in columnNames)
            {
                if (columnName != xmlName) { continue; }

                var propertyValue = row[columnName];
                if (propertyValue != DBNull.Value)
                {
                    ParsePrimitive(prop, entity, row[columnName]);
                    break;
                }
            }
        }

        public static string[] GetDataColumnNames(DataTable table)
        {
            return table.Columns
                 .Cast<DataColumn>()
                 .Select(x => x.ColumnName)
                 .ToArray();
        }

        private static void ParsePrimitive(PropertyInfo prop, object entity, object value)
        {
            try
            {
                if (value == null)
                {
                    prop.SetValue(entity, null, null);
                }
                else if (prop.PropertyType == typeof(string))
                {
                    prop.SetValue(entity, value.ToString(), null);
                }
                else if (prop.PropertyType == typeof(short) || prop.PropertyType == typeof(short?))
                {
                    if (short.TryParse(value.ToString(), out var shortValue))
                    {
                        prop.SetValue(entity, shortValue, null);
                    }
                }
                else if (prop.PropertyType == typeof(int) || prop.PropertyType == typeof(int?))
                {
                    if (int.TryParse(value.ToString(), out var intValue))
                    {
                        prop.SetValue(entity, intValue, null);
                    }
                }
                else if (prop.PropertyType == typeof(long) || prop.PropertyType == typeof(long?))
                {
                    if (long.TryParse(value.ToString(), out var longValue))
                    {
                        prop.SetValue(entity, longValue, null);
                    }
                }
                else if (prop.PropertyType == typeof(double) || prop.PropertyType == typeof(double?))
                {
                    if (double.TryParse(value.ToString(), out var doubleValue))
                    {
                        prop.SetValue(entity, doubleValue, null);
                    }
                }
                else if (prop.PropertyType == typeof(float) || prop.PropertyType == typeof(float?))
                {
                    if (float.TryParse(value.ToString(), out var floatValue))
                    {
                        prop.SetValue(entity, floatValue, null);
                    }
                }
                else if (prop.PropertyType == typeof(decimal) || prop.PropertyType == typeof(decimal?))
                {
                    if (decimal.TryParse(value.ToString(), out var decimalValue))
                    {
                        prop.SetValue(entity, decimalValue, null);
                    }
                }
                else if (prop.PropertyType == typeof(DateTime) || prop.PropertyType == typeof(DateTime?))
                {
                    ParseDateTime(prop, entity, value.ToString());
                }
                else if (prop.PropertyType == typeof(bool) || prop.PropertyType == typeof(bool?))
                {
                    prop.SetValue(entity, ParseBoolean(value), null);
                }
                else if (prop.PropertyType == typeof(Guid))
                {
                    if (Guid.TryParse(value.ToString(), out var guidValue))
                    {
                        prop.SetValue(entity, guidValue, null);
                    }
                    else if (Guid.TryParseExact(value.ToString(), "B", out guidValue))
                    {
                        prop.SetValue(entity, guidValue, null);
                    }
                }
                else
                {
                    prop.SetValue(entity, value, null);
                }
            }
            catch(Exception ex)
            {
                //ignore
            }
        }

        public static void ParseDateTime(PropertyInfo prop, object entity, object value)
        {
            if (DateTime.TryParse(value.ToString(), out var date))
            {
                prop.SetValue(entity, date, null);
            }
            else if (DateTime.TryParseExact(value.ToString(), "MMddyyyy",
                new CultureInfo("en-US"), DateTimeStyles.AssumeLocal, out date))
            {
                prop.SetValue(prop, date, null);
            }
            else if (DateTime.TryParseExact(value.ToString().Replace("/", ".").Replace("-", "."), "dd.MM.yyyy",
                DateTimeFormatInfo.CurrentInfo, DateTimeStyles.None, out date))
            {
                prop.SetValue(prop, date, null);
            }
        }

        public static bool ParseBoolean(object value)
        {
            if (value == null || value == DBNull.Value) return false;

            return value.ToString().ToLowerInvariant() switch
            {
                "1" or "y" or "yes" or "true" => true,
                _ => false,
            };
        }
    }
}
