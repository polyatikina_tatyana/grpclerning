﻿using System.Xml;
using System.Xml.Serialization;

namespace GrpcServiceApp.Domain.Extensions;

internal static class AttributeHelper
{
    public static string GetXmlName(Type type, string propertyName)
    {
        var property = type.GetProperty(propertyName)
                           .GetCustomAttributes(false)
                           .Where(x => x.GetType().Name == nameof(XmlAttributeAttribute))
                           .FirstOrDefault();
        return property != null ? ((XmlAttributeAttribute)property).AttributeName : null;
    }
}
