﻿using System.Net.Http;

namespace GrpcServiceApp.Domain.Extensions
{
    public static class HttpClientExtensions
    {
        public static Task DownloadFileAsync(this HttpClient client, string uri, string fileName, bool overwrite = true)
        {
            return DownloadFileAsync(client, new Uri(uri), fileName, overwrite);
        }

        public static async Task DownloadFileAsync(this HttpClient client, Uri uri, string fileName, bool overwrite = true)
        {
            string pathname = Path.GetFullPath(fileName);

            if (!overwrite && File.Exists(fileName))
            {
                throw new InvalidOperationException(string.Format("File {0} already exists.", pathname));
            }

            using var stream = await client.GetStreamAsync(uri);
            using var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write);
            await stream.CopyToAsync(fs);
            await fs.FlushAsync();
        }

        public static async Task WriteToFileAsync(this HttpClient client, Uri uri, string fileName, bool overwrite = true)
        {
            string pathname = Path.GetFullPath(fileName);

            if (!overwrite && File.Exists(fileName))
            {
                throw new InvalidOperationException(string.Format("File {0} already exists.", pathname));
            }

            var httpResponse = await client.GetAsync(uri);

            var successResponse = httpResponse.EnsureSuccessStatusCode();

            if (successResponse.IsSuccessStatusCode)
            {
                HttpContent content = successResponse.Content;
                using var contentStream = await content.ReadAsStreamAsync(); // get the actual content stream
                using var fs = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None);
                await contentStream.CopyToAsync(fs);
                await fs.FlushAsync();
            }
            else
            {
                throw new FileNotFoundException();
            }
        }
    }
}
