﻿using GrpcServiceApp.Domain.Mappers;
using System.Data;
using System.Reflection;
using System.Xml;

namespace GrpcServiceApp.Domain.Extensions
{
    public static class FiasDataReader
    {
        private static DataSet GetXmlDataSet(string version, string xsdName)
        {
            var dataSet = new DataSet();

            var settings = new XmlReaderSettings
            {
                DtdProcessing = DtdProcessing.Parse
            };

            var assembly = Assembly.GetExecutingAssembly();
            var xsdFullName = $"{assembly.GetName().Name}.XmlModels.{version}.{xsdName}";
            var resourceName = assembly.GetManifestResourceNames()
                                       .FirstOrDefault(x => x.Contains(xsdFullName, StringComparison.InvariantCultureIgnoreCase));
            if (resourceName == null) return dataSet;

            using Stream stream = assembly.GetManifestResourceStream(resourceName);
            using XmlReader reader = XmlReader.Create(stream, settings);

            dataSet.ReadXmlSchema(reader);

            return dataSet;
        }

        private static DataSet LoadFromXml(Stream stream, string version, string xsdName)
        {
            var dataSet = GetXmlDataSet(version, xsdName);

            dataSet.ReadXml(stream);

            return dataSet;
        }

        public static DataTable LoadTableFromXml(Stream stream, string version, string xsdName, string tableName)
        {
            var ds = LoadFromXml(stream, version, xsdName);

            if (ds.Tables.Count == 0) return null;

            return ds.Tables[tableName];
        }

        public static IEnumerable<TEntity> LoadFromXml<TEntity>(Stream stream, string version, string xsdName, string tableName)
            where TEntity : class, new()
        {
            return LoadTableFromXml(stream, version, xsdName, tableName)?.Map<TEntity>();
        }
    }
}
